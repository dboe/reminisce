#!/usr/bin/env python

"""Tests for `reminisce` package."""

import unittest
import reminisce


class TestPackage(unittest.TestCase):
    """Tests for `reminisce` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_version_type(self):
        """Assure that version type is str."""

        self.assertIsInstance(reminisce.__version__, str)
