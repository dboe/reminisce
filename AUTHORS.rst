=======
Credits
=======
This package was created with Cookiecutter_ and the `dboe/dough`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`dboe/dough`: https://gitlab.com/dboe/dough

Development Lead
----------------

* Daniel Böckenhoff <dboe@ipp.mpg.de>

Contributors
------------

None yet. Why not be the first?
