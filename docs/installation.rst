.. highlight:: shell

============
Installation
============


Stable release
--------------

To install reminisce, run this command in your terminal:

.. code-block:: console

    $ pip install reminisce

This is the preferred method to install reminisce, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/



From sources
------------

The sources for reminisce can be downloaded from the `remote repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/dboe/reminisce

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/dboe/reminisce/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _remote repo: https://gitlab.com/dboe/reminisce
.. _tarball: https://gitlab.com/dboe/reminisce/tarball/master
