===========================
Introduction to `reminisce`
===========================


.. pypi
.. image:: https://img.shields.io/pypi/v/reminisce.svg
    :target: https://pypi.python.org/pypi/reminisce
.. ci
    .. image:: https://img.shields.io/travis/dboe/reminisce.svg
        :target: https://travis-ci.com/dboe/reminisce
.. image:: https://gitlab.com/dboe/reminisce/badges/master/pipeline.svg
    :target: https://gitlab.com/dboe/reminisce/commits/master

.. coverage
.. image:: https://gitlab.com/dboe/reminisce/badges/master/coverage.svg
    :target: https://gitlab.com/dboe/reminisce/commits/master

.. readthedocs
.. image:: https://readthedocs.org/projects/reminisce/badge/?version=latest
    :target: https://reminisce.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. pyup crosschecks your dependencies. Github is default, gitlab more complicated: https://pyup.readthedocs.io/en/latest/readme.html#run-your-first-update 
    .. image:: https://pyup.io/repos/github/dboe/reminisce/shield.svg
        :target: https://pyup.io/repos/github/dboe/reminisce/
        :alt: Updates

.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :target: https://github.com/pre-commit/pre-commit
   :alt: pre-commit


This project manages your media files by meta data.


Licensed under the ``MIT License``

Resources
---------

* Source code: https://gitlab.com/dboe/reminisce
* Documentation: https://reminisce.readthedocs.io
* Pypi: https://pypi.python.org/pypi/reminisce


Features
--------

The following features should be highlighted:

* TODO
